import { MarkupHighLight } from '../src/lib/markupHighLight';

describe('Test the MarkUpHighLight Library', (): void => {
  it('1. Check for empty search text with non-empty search key with case insensitive flag turned on', (): void => {
    const searchText = '';
    const searchKey = 'he';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('');
  });
  it('2. Check for empty search text with non-empty search key with case insensitive flag turned off', (): void => {
    const searchText = '';
    const searchKey = 'he';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('');
  });
  it('3. Check for empty search text with empty search key with case insensitive flag turned on', (): void => {
    const searchText = '';
    const searchKey = '';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('');
  });
  it('4. Check for empty search text with empty search key with case insensitive flag turned off', (): void => {
    const searchText = '';
    const searchKey = '';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('');
  });
  it('5. Check for non-empty search text with non-empty search key with case insensitive flag turned on', (): void => {
    const searchText = 'Sample String One';
    const searchKey = 'str';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('Sample <mark>Str</mark>ing One');
  });
  it('6. Check for non-empty search text with non-empty search key with case insensitive flag turned off', (): void => {
    const searchText = 'Sample String One';
    const searchKey = 'str';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('Sample String One');
  });
  it('7. Check for list of empty search text with non-empty search key with case insensitive flag turned on', (): void => {
    const searchText = ['', '', ''];
    const searchKey = 'he';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual(['', '', '']);
  });
  it('8. Check for list of empty search text with non-empty search key with case insensitive flag turned off', (): void => {
    const searchText = ['', '', ''];
    const searchKey = 'he';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual(['', '', '']);
  });
  it('9. Check for list of empty search text with empty search key with case insensitive flag turned on', (): void => {
    const searchText = ['', '', ''];
    const searchKey = '';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual(['', '', '']);
  });
  it('10. Check for list of empty search text with empty search key with case insensitive flag turned off', (): void => {
    const searchText = ['', '', ''];
    const searchKey = '';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual(['', '', '']);
  });
  it('11. Check for list of non-empty search text with non-empty search key with case insensitive flag turned on', (): void => {
    const searchText = ['Sample String One', 'Sample String two', 'Sample String Three'];
    const searchKey = 'str';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual([
      'Sample <mark>Str</mark>ing One',
      'Sample <mark>Str</mark>ing two',
      'Sample <mark>Str</mark>ing Three',
    ]);
  });
  it('12. Check for list of non-empty search text with non-empty search key with case insensitive flag turned off', (): void => {
    const searchText = ['Sample String One', 'Sample String two', 'Sample String Three'];
    const searchKey = 'str';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual(['Sample String One', 'Sample String two', 'Sample String Three']);
  });
  it('13. Check for list of empty & non-empty search text with non-empty search key with case insensitive flag turned on', (): void => {
    const searchText = ['Sample String One', '', 'Sample String Three'];
    const searchKey = 'str';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual(['Sample <mark>Str</mark>ing One', '', 'Sample <mark>Str</mark>ing Three']);
  });
  it('14. Check for list of empty & non-empty search text with non-empty search key with case insensitive flag turned off', (): void => {
    const searchText = ['Sample String One', '', 'Sample String Three'];
    const searchKey = 'str';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual(['Sample String One', '', 'Sample String Three']);
  });
  it('15. Check for multi match in search text for non-empty search key with case insensitive flag turned on', (): void => {
    const searchText = 'pAnama';
    const searchKey = 'a';
    const markupObject = new MarkupHighLight(searchText, searchKey);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('p<mark>A</mark>n<mark>a</mark>m<mark>a</mark>');
  });
  it('16. Check for multi match in search text for non-empty search key with case insensitive flag turned off', (): void => {
    const searchText = 'pAnama';
    const searchKey = 'a';
    const markupObject = new MarkupHighLight(searchText, searchKey, false);
    const result = markupObject.getHighLightedText();
    expect(result).toEqual('pAn<mark>a</mark>m<mark>a</mark>');
  });
});
