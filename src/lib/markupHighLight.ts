export class MarkupHighLight {
  private _textToBeHighLighted: string | string[];
  private _searchKey: string;
  private _caseInSensitive: boolean;
  constructor(text: string | string[] = '', searchKey: string = '', caseinSensitive: boolean = true) {
    this._textToBeHighLighted = text;
    this._searchKey = searchKey;
    this._caseInSensitive = caseinSensitive;
  }

  public getTextToBeHighLighted(): string | string[] {
    return this._textToBeHighLighted;
  }

  public setTextToBeHighLighted(text: string | string[]): void {
    this._textToBeHighLighted = text;
  }

  public getSearchKey(): string {
    return this._searchKey;
  }

  public setSearchKey(key: string): void {
    this._searchKey = key;
  }

  public isCaseInSensitiveEnabled(): boolean {
    return this._caseInSensitive;
  }

  public toggleCaseInSensitiveEnabled(ignoreCase: boolean): void {
    this._caseInSensitive = ignoreCase;
  }

  private getMatchingIndexes(tempText: string, searchKey: string): number[] {
    let regex;
    let result;
    const indices = [];
    if (this._caseInSensitive) {
      regex = new RegExp(searchKey.toLowerCase(), 'g');
    } else {
      regex = new RegExp(searchKey, 'g');
    }
    const regexp = new RegExp(regex);
    if (this._caseInSensitive) {
      // tslint:disable-next-line: no-conditional-assignment
      while ((result = regexp.exec(tempText.toLowerCase()))) {
        indices.push(result.index);
      }
    } else {
      // tslint:disable-next-line: no-conditional-assignment
      while ((result = regexp.exec(tempText))) {
        indices.push(result.index);
      }
    }
    return indices;
  }

  private isListOfEmptyStrings(): boolean {
    const emptyStringList = (this._textToBeHighLighted as string[]).filter((str: string): boolean => str === '');
    if (emptyStringList.length === this._textToBeHighLighted.length) {
      return true;
    }
    return false;
  }

  public getHighLightedText(): string | string[] {
    if (this._searchKey === '') {
      return this._textToBeHighLighted;
    }
    if (this._textToBeHighLighted === '' || this._textToBeHighLighted.length === 0) {
      return '';
    }
    if (!Array.isArray(this._textToBeHighLighted) && !(typeof this._textToBeHighLighted === 'string')) {
      return '';
    }

    if (typeof this._textToBeHighLighted === 'string') {
      return this.getHighLightedString(this._textToBeHighLighted);
    } else {
      if (this.isListOfEmptyStrings()) {
        return this._textToBeHighLighted.map((): string => '');
      }
      return this.getListOfHighLightedStrings(this._textToBeHighLighted);
    }
  }

  private setCharAt(str: string, index: number, chr: string): string {
    if (index > str.length - 1) {
      return str;
    }
    return str.substr(0, index) + chr + str.substr(index + this._searchKey.length);
  }

  private getHighLightedString(stringToBeHighLighted: string): string {
    let matchingIndexes: number[] = [];
    matchingIndexes = this.getMatchingIndexes(stringToBeHighLighted, this._searchKey);
    if (matchingIndexes.length === 0) {
      return stringToBeHighLighted;
    }
    let matchIdx = 0;
    let offset = 0;
    let temp = '';
    let replaceChar = '';
    while (matchIdx < matchingIndexes.length) {
      temp = stringToBeHighLighted.substr(matchingIndexes[matchIdx] + offset, this._searchKey.length);
      if (temp === '') {
        continue;
      }
      replaceChar = `<mark>${temp}</mark>`;
      stringToBeHighLighted = this.setCharAt(stringToBeHighLighted, matchingIndexes[matchIdx] + offset, replaceChar);
      offset = offset + replaceChar.length - 1;
      matchIdx++;
    }
    return stringToBeHighLighted;
  }

  private getListOfHighLightedStrings(stringsToBeHighLighted: string[]): string[] {
    const finalHighLightedString: string[] = [];
    stringsToBeHighLighted.forEach((stringToBeHighLighted: string): void => {
      finalHighLightedString.push(this.getHighLightedString(stringToBeHighLighted));
    });
    return finalHighLightedString;
  }

  public reset(): void {
    this._textToBeHighLighted = '';
    this._searchKey = '';
    this._caseInSensitive = true;
  }
}
