# Markup Highlight

![Bitbucket Pipelines branch](https://img.shields.io/bitbucket/pipelines/abhinavstyquo1989/markup_highlight/master?style=plastic)


[![install size](https://packagephobia.now.sh/badge?p=env-explorer)](https://packagephobia.now.sh/result?p=markup_highlight)

## Introduction

Markup Highlight is a library, which will process a string or a set of strings against a search key and modify the input string by highlighting the matched part from the input strings.

---

## Steps to use

1. Install the dependency.

   `npm i markup_highlight`

2. Import the `MarkupHighLight` class in your module or component, as shown in code snippet below,

   ```typescript
   import { MarkupHighLight } from 'markup-highlight';
   ```

3. Invoke the constructor `MarkupHighLight(searchText, searchKey)` to set the intial properties.

   ```typescript
   const markupObject = new MarkupHighLight(searchText, searchKey);
   ```

4. After setting the values in constructor, invoke `getHighLightedText()` method to get the highlighted.
   ```typescript
   const result = markupObject.getHighLightedText();
   ```

---

## Examples

1. For usage in `Typescript`, please refer
   [here](https://stackblitz.com/edit/typescript-3497xu?file=index.ts).

2. For Usage in `Angular` application, please refer [here](https://stackblitz.com/edit/angular-14wopb).

---